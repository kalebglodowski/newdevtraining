﻿$(document).ready(function () {

    $("#addUser").on("click", AddUserClick); //when you click "addUser" made in users.html, then call function AddUserClick
    $("#getUser").on("click", GetUserClick); //when you click "getUser" made in users.html, then call function GetUserClick
});

function GetUserClick() {
    var id = $("#personId").val();          //on a GET, need to place in a query string
    $.ajax({
        url: "/api/Person?id=" + id,
        type: "GET",
        success: GetPersonSuccess,
        error: GetPersonError,
    })
}
function AddUserClick() {

    var myPerson = {}
    myPerson.firstName = $("#firstName").val();
    myPerson.lastName = $("#lastName").val();

    $.ajax({
        url: "/api/Person",             //pulling from the controller 
        type: "POST",                   //
        data: myPerson,                 //creating the function later
        success: AddPersonSuccess,      //''
        error: AddPersonError           //''
    });
}

function AddPersonSuccess() { console.log(data); };
function AddPersonError() { };

function GetPersonSuccess(data) {
    $("#firstName").val(data.firstName);
    $("#lastName").val(data.lastName);
};
function GetPersonError() { };
