﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CounterpartProject.Data;
using CounterpartProject.Models;


namespace CounterpartProject.DataClasses
{
    public class PersonData
    {
        public static int AddPerson(MyPerson addMe)       //function to add a person
        {
            using( var db = new KalebTrainingEntities1()) //creating a new connection to the database
            {
                Person newPerson = new Person();       //Class localVariable = object Person (Person.person)
                newPerson.firstName = addMe.firstName; //pulling firstName from /Models>MyPerson class
                newPerson.lastName = addMe.lastName;   // ''
                db.People.Add(newPerson);              //adding to the database
                db.SaveChanges();                      
                return newPerson.id;                   //creating a new ID, ensure database has auto-incrimenting (column properties>identity specification>yes,1,1)
            }
        }
        public static MyPerson GetPerson(int id)       //function to pull a person via ID, return everything connected to MyPerson
        {
            using (var db = new KalebTrainingEntities1()) //creating a new connection to the database
            {
                var myPerson = db.People.Where(x => x.id == id).FirstOrDefault();
                return new MyPerson() { firstName = myPerson.firstName, lastName = myPerson.lastName };
            }
        }
    }
}