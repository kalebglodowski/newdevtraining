﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CounterpartProject.Models;
using CounterpartProject.DataClasses;

namespace CounterpartProject.Controllers
{
    public class PersonController : ApiController
    {
        //GET - retrieves an object from the database
        //POST - adds an object to the database
        //PUT - updates an objetc
        //DELETE - deletes or de-activates/inactivates

        [HttpGet] //tag that tells it is a Get
        public HttpResponseMessage Get(int id)
        {
            var returnMe = PersonData.GetPerson(id);
            return Request.CreateResponse(HttpStatusCode.OK, returnMe); //older way to do a get, seems more logical but longer
        }

        [HttpPost]
        public HttpResponseMessage CreatePerson(MyPerson addMe) //can only pass in 1 object, cannot pass in first/last
        {
            var myNewPersonsID = PersonData.AddPerson(addMe);                 //declaring variable
            return Request.CreateResponse(HttpStatusCode.OK, myNewPersonsID); //returning line 22 under Get. Second parameter is the message returned to the server.
        }
    }
}
