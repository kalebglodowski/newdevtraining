
CREATE TABLE [dbo].[EmailLog](
	[id] [bigint] IDENTITY(1,1) primary key NOT NULL,
	[recipient] [varchar](255) NOT NULL,
	[subject] [varchar](255) NOT NULL,
	[sentDate] [datetime] NOT NULL,
	[status] [varchar](max) NULL,
)
GO

CREATE TABLE [dbo].[Roles](
	[id] [int] IDENTITY(1,1) primary key NOT NULL,
	[roleName] [varchar](50) NULL,
)
GO

CREATE TABLE [dbo].[States](
	[id] [int] NOT NULL primary key,
	[abbreviation] [varchar](5) NOT NULL,
	[name] [varchar](30) NOT NULL,
)
GO

CREATE TABLE [dbo].[UserProfile](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[email] [varchar](200) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[isActive] [bit] NOT NULL,
	[impersonating] [int] NULL,
	[homePhone] [varchar](20) NULL,
	[birthday] [datetime] NULL,
	[address] [varchar](255) NULL,
	[city] [varchar](255) NULL,
	[state] [varchar](50) NULL,
	[zip] [varchar](10) NULL,
	[comments] [varchar](max) NULL,
	[lastUpdatedDate] [datetime] NULL
)
GO

CREATE TABLE [dbo].[UserProfileLinks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](255) NOT NULL,
	[linkType] [varchar](10) NULL,
	[linkExpiration] [datetime] NULL,
	[userId] [int] NOT NULL,
)
GO

CREATE TABLE [dbo].[UserRoles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[roleId] [int] NOT NULL,
)
GO
ALTER TABLE [dbo].[UserProfile] ADD  DEFAULT ((1)) FOR [isActive]
GO
ALTER TABLE [dbo].[UserProfileLinks]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([id])
GO
ALTER TABLE [dbo].[UserProfileLinks]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([id])
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD FOREIGN KEY([roleId])
REFERENCES [dbo].[Roles] ([id])
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([id])
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([id])
GO

INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (1, N'AL', N'Alabama')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (2, N'AK', N'Alaska')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (3, N'AZ', N'Arizona')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (4, N'AR', N'Arkansas')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (5, N'CA', N'California')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (6, N'CO', N'Colorado')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (7, N'CT', N'Connecticut')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (8, N'DE', N'Delaware')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (9, N'FL', N'Florida')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (10, N'GA', N'Georgia')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (11, N'HI', N'Hawaii')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (12, N'ID', N'Idaho')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (13, N'IL', N'Illinois')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (14, N'IN', N'Indiana')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (15, N'IA', N'Iowa')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (16, N'KS', N'Kansas')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (17, N'KY', N'Kentucky')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (18, N'LA', N'Louisiana')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (19, N'ME', N'Maine')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (20, N'MD', N'Maryland')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (21, N'MA', N'Massachusetts')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (22, N'MI', N'Michigan')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (23, N'MN', N'Minnesota')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (24, N'MS', N'Mississippi')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (25, N'MO', N'Missouri')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (26, N'MT', N'Montana')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (27, N'NE', N'Nebraska')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (28, N'NV', N'Nevada')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (29, N'NH', N'New Hampshire')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (30, N'NJ', N'New Jersey')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (31, N'NM', N'New Mexico')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (32, N'NY', N'New York')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (33, N'NC', N'North Carolina')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (34, N'ND', N'North Dakota')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (35, N'OH', N'Ohio')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (36, N'OK', N'Oklahoma')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (37, N'OR', N'Oregon')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (38, N'PA', N'Pennsylvania')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (39, N'RI', N'Rhode Island')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (40, N'SC', N'South Carolina')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (41, N'SD', N'South Dakota')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (42, N'TN', N'Tennessee')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (43, N'TX', N'Texas')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (44, N'UT', N'Utah')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (45, N'VT', N'Vermont')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (46, N'VA', N'Virginia')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (47, N'WA', N'Washington')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (48, N'WV', N'West Virginia')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (49, N'WI', N'Wisconsin')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (50, N'WY', N'Wyoming')
GO
INSERT [dbo].[States] ([id], [abbreviation], [name]) VALUES (51, N'DC', N'District of Columbia')
GO

CREATE TABLE [dbo].[AuditLog](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[userProfileId] [int] NOT NULL,
	[fieldChanged] [varchar](50) NULL,
	[changedFrom] [varchar](max) NULL,
	[changedTo] [varchar](max) NULL,
	[objectType] [varchar](50) NULL,
	[relatedObjectId] [int] NOT NULL,
	[eventDate] [datetime] NOT NULL,
	[detail] [varchar](max) NULL,
)
GO

ALTER TABLE [dbo].[AuditLog] ADD  DEFAULT (getdate()) FOR [eventDate]
GO

ALTER TABLE [dbo].[AuditLog]  WITH CHECK ADD FOREIGN KEY([userProfileId])
REFERENCES [dbo].[UserProfile] ([id])
GO

